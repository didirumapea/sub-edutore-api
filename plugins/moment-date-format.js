//
const moment = require('moment')

function MOMENT_CONSTRUCTOR() {
    this.utc7 = () => {
        return moment.utc().utcOffset('+07:00').format("YYYY-MM-DD HH:mm:ss")
    }
    this.localDate = () => {
        return moment().format("YYYY-MM-DD HH:mm:ss")
    }

    this.longLocalDate = () => {
        return moment().format("YYYY-MM-DD")
    }

    this.dateLast7days = () => {
        return moment.utc().subtract(7, 'days').utcOffset('+07:00').format("YYYY-MM-DD HH:mm:ss")
    }
    this.timezoneFormat = () => {
        return moment().format()
    }

    this.utc = () => {
        return moment().utc().format('YYYY-MM-DD HH:mm:ss')
    }

    this.unix = () => {
        return moment().format('X')
    }

    this.utcOffset = '+07:00'
}

module.exports = new MOMENT_CONSTRUCTOR()

// module.exports = {
//     utcOffset: '+07:00',
//     utcTime: new utcTime7()
// };