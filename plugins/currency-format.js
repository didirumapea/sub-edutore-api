exports.currencyFormat = (n, currency) => {
    // WITH COMMA DIVIDER
    return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}