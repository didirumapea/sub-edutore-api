require('dotenv').config()
const express = require('express');
const cors = require('cors');
const app = express();
const apiRoute = require('./routes/api');
const version = 'v1/'
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const checkHeader = require('./middleware/check-header')
app.use(cors());
// const sshtunnel = require('./database/ssh-tunnel-db')
// Tambahkan ini jika terjadi CORS ACCESS CONTROL BLOCKED
// app.use(function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, On-Project, Signature-Key");
//     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE');
//     // Website you wish to allow to connect
//     // res.header('Access-Control-Allow-Origin', 'https://staging.edutore.com');
//     next();
// });
// use it before all route definitions
app.use(express.static('/mnt/cdn'));

app.get('/', function (req, res) {
    res.send('Hello Sub Edutore API\n')
})

app.get('/files', function (req, res) {
    res.send('Hello CDN  Sub Edutore!\n')
})

app.get('/api/'+version, function (req, res) {
    res.send('Hello Sub Edutore API With Version\n')
})

// catch 404 and forward to error handler global
// app.use(function(req, res, next) {
//     return res.status(404).json({
//         success : 'false',
//         message :'Request tidak di Temukan'
//     });
//     let err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });


app.use(bodyParser.urlencoded({
    extended: true,
    // limit: '50mb'
}));
app.use(bodyParser.json({
    // limit: '50mb'
}));


app.use('/api/'+version, checkHeader, apiRoute); //ex localhost:3002/api/merchant/get/list/1


app.use(cookieParser());
// app.listen('3002')
const PORT = process.env.PORT || process.env.PORT
app.listen(PORT, console.log(`Server started on port ${PORT}`))