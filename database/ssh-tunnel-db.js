const Ssh  = require('node-ssh');
const tunnel = require('tunnel-ssh');
let knexLib = require('knex')

// check ssh connection
async function main() {
    const ssh = new Ssh();

    const sshPassword = 'Member2018';

    // connect
    await ssh.connect({
        host: '206.189.157.62',
        port: 22,
        username: 'rim',
        password: sshPassword
    });

    // execute command
    res = await ssh.execCommand('ls -al', {options: {pty: true}});
    // const res2 = await ssh.execCommand('sudo ls /var/log/httpd', {stdin: sshPassword + '\n', options: {pty: true}});
    // console.log(res);
    console.log('connected to ssh')
    // disconnect
    ssh.dispose();
}

async function ssh_tunnel() {
    const sshUserName = 'rim';
    const sshPassword = 'Member2018';

    // rimember
    let sshConfig = {
        host: '206.189.157.62',
        port: 22,
        username: sshUserName,
        password: sshPassword,
        keepaliveInterval: 60000,
        keepAlive: true,
        dstHost: 'localhost', // host db
        dstPort: '3306', // port db
        localHost: 'localhost',
        localPort: '45234' // create anything opened port / random
    };

    // topskor
    let sshConfig2 = {
        host: '134.209.98.193',
        port: 22,
        username: 'root',
        password: 'happyc0d1ng',
        keepaliveInterval: 60000,
        keepAlive: true,
        dstHost: '178.128.82.86', // host db
        dstPort: '3306', // port db
        localHost: '178.128.82.86',
        localPort: '45234' // create anything opened port / random
    };

    const tnl = tunnel(sshConfig, async (err, server) => {
        if (err) {
            // console.log('error')
            throw err;
        }
        console.log('connected to ssh tunnel')
        let knex = knexLib({
            client: 'mysql',
            connection: {
                // host: 'localhost',
                database: 'korandigital',
                port:     45234,
                user:     'gadogado',
                password: 'h4ng4tOK!'
            }
        });
        const rows = await knex
            .select()
            .from('t_halamanstatis')
            .limit(1);
        console.log(rows.length);
        console.log('connected to db')
        // write what you want to do through tunnel.
        await knex.destroy();

        tnl.close();
    });
}

// main();
ssh_tunnel();
