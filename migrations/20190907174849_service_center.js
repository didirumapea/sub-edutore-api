
exports.up = function(knex, Promise) {
    return knex.schema.createTable("service_center", (table) => {
        table.increments()
        table.string('name').notNullable();
        table.string('address').notNullable();
        table.string('city').notNullable();
        table.json('coordinat').notNullable();
        table.string('phone').notNullable();
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('service_center')
};
