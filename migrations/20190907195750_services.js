
exports.up = function(knex, Promise) {
    return knex.schema.createTable("services", (table) => {
        // increment is auto added unsigned
        table.increments()
        table.string('name')
        table.string('type')
        table.string('detail')
        table.string('img_url')
        table.integer('price')
        table.integer('devices_type_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('device_type')
            .onDelete('CASCADE')
            .index()
        table.enum('service_type', ['on_site', 'service_center']).notNullable();
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('services')
};
