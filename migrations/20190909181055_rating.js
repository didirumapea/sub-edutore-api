
exports.up = function(knex, Promise) {
    return knex.schema.createTable("rating", (table) => {
        // increment is auto added unsigned
        table.increments()
        table.enum('rating', [1, 2, 3, 4, 5]);
        table.integer('order_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('order')
            .onDelete('CASCADE')
            .index()
        table.integer('technician_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('technician')
            .onDelete('CASCADE')
            .index()
        table.integer('user_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('users')
            .onDelete('CASCADE')
            .index()
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('rating')
};
