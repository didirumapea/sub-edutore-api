
exports.up = function(knex, Promise) {
    return knex.schema.createTable("payment_provider", (table) => {
        // increment is auto added unsigned
        table.increments()
        table.string('name')
        table.string('address')
        table.string('company_name')
        table.string('type')
        table.string('token')
        table.enum('status', [0, 1]);
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('payment_provider')
};
