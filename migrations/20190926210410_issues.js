
exports.up = function(knex, Promise) {
    return knex.schema.createTable("issues", (table) => {
        table.increments()
        table.string('name').notNullable();
        table.string('detail').notNullable();
        table.integer('price').notNullable();
        table.integer('devices_type_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('device_type')
        .onDelete('CASCADE')
        .index()
        table.integer('activated').notNullable().defaultTo(1);
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('issues')
};
