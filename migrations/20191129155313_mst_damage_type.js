
exports.up = function(knex, Promise) {
    return knex.schema.createTable("mst_damage_type", (table) => {
        table.increments()
        table.string('name').notNullable();
        table.integer('activated').notNullable().defaultTo(1);
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('mst_damage_type')
};
