
exports.up = function(knex) {
    return knex.schema.table("mst_damage_type", (table) => {
        table.enum('service_type', ['on_site', 'service_center'])
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('mst_damage_type', (table) => {
        table.enum('service_type', ['on_site', 'service_center'])
            .notNullable()
            .defaultTo('service_center')
            .alter();
    })
};
