
exports.up = function(knex) {
    return knex.schema.table("damage_type", (table) => {
        table.text('name')
            .notNullable()
            .alter();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('damage_type', (table) => {
        table.string('name')
            .alter();
    })
};
