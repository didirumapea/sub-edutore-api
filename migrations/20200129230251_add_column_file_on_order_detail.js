
exports.up = function(knex, Promise) {
    return knex.schema.alterTable("order_details", (table) => {
        table.json('files')
            .after('total_price')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable("order_details", (table) => {
        table.dropColumn('files');
    })
};
