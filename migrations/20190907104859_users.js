
exports.up = function(knex, Promise) {
    return knex.schema.createTable("users", (table) => {
        // increment is auto added unsigned
        table.increments()
        table.string('email')
        table.string('password')
        table.string('name')
        table.string('img_url')
        table.string('phone')
        table.string('social_token')
        table.enum('register_type', ['fb', 'google', 'manual']);
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users')
};
