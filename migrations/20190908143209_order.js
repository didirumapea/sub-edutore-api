
exports.up = function(knex, Promise) {
    return knex.schema.createTable("order", (table) => {
        table.increments()
        table.integer('trans_id')
            .unsigned()
            .references('id')
            .inTable('transaction')
            .onDelete('CASCADE')
            .index();
        table.integer('user_id').notNullable()
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('users')
            .onDelete('CASCADE')
            .index();
        table.string('no_order').notNullable();
        table.integer('order_detail_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('order_details')
            .onDelete('CASCADE')
            .index();
        table.integer('technician_id')
            .unsigned()
            .references('id')
            .inTable('technician')
            .onDelete('CASCADE')
            .index();
        table.enum('service_type', ['on_site', 'service_center']).notNullable();
        table.integer('service_center_id')
            .notNullable()
            .unsigned()
            .references('id')
            .inTable('service_center')
            .onDelete('CASCADE')
            .index();
        table.enum('payment_status', ['pending', 'paid']).notNullable();
        table.dateTime('finish_date');
        table.dateTime('order_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('order')
};
