
exports.up = function(knex, Promise) {
    return knex.schema.createTable("device_brand", (table) => {
        // increment is auto added unsigned
        table.increments()
        table.string('name')
        table.string('img_url')
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('device_brand')
};
