
exports.up = function(knex, Promise) {
    return knex.schema.createTable("transaction", (table) => {
        table.increments()
        table.string('no_order').notNullable();
        table.string('transaction_id')
        table.enum('transaction_status', ['settlement', 'pending', 'failure']);
        table.string('signature_key');
        table.string('fraud_status');
        table.string('payment_type');
        table.string('bank_name');
        table.string('va_number');
        table.string('status_code');
        table.string('redirect_url');
        table.integer('total');
        table.enum('isDeleted', [0, 1])
            .notNullable()
            .defaultTo(0);
        table.dateTime('paid_date');
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('transaction')
};
