
exports.up = function(knex, Promise) {
    return knex.schema.alterTable("damage_type", (table) => {
        table.integer('price')
            .notNullable()
            .after('name')
            .defaultTo(0);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable("damage_type", (table) => {
        table.dropColumn('price');
    })
};
