
exports.up = function(knex) {
    return knex.schema.table('sparepart', (table) => {
        table.renameColumn('damage_type_id', 'mst_damage_type_id')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('sparepart', (table) => {
        table.renameColumn('mst_damage_type_id', 'damage_type_id')
    })
};
