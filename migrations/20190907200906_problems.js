
exports.up = function(knex, Promise) {
    return knex.schema.createTable("problems", (table) => {
        // increment is auto added unsigned
        table.increments()
        table.integer('order_detail_id')
            .notNullable()
            .unsigned()
            .references('id')
            .inTable('order_details')
            .onDelete('CASCADE')
            .index()
        table.integer('sparepart_id')
            .unsigned()
            .references('id')
            .inTable('sparepart')
            .onDelete('CASCADE')
            .index()
        table.integer('services_id')
            .unsigned()
            .references('id')
            .inTable('services')
            .onDelete('CASCADE')
            .index()
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    }).catch((err) => {
        console.log(err)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('problems')
};
