
exports.up = function(knex, Promise) {
    return knex.schema.createTable("admin", (table) => {
        table.increments()
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.string('phone').notNullable();
        table.text('address').notNullable();
        // table.integer('service_center_id')
        //     .unsigned()
        //     .notNullable()
        //     .references('id')
        //     .inTable('service_center')
        //     .onDelete('CASCADE')
        //     .index()
        table.integer('activated').notNullable().defaultTo(1);
        table.enum('isDeleted', [0, 1]).notNullable().defaultTo(0);
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('admin')
};
