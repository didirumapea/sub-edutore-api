
exports.up = function(knex, Promise) {
    return knex.schema.table("mst_damage_type", (table) => {
        table.enum('service_type', ['on_site', 'service_center'])
            .notNullable()
            .after('name')
            .defaultTo('service_center');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table("mst_damage_type", (table) => {
        table.dropColumn('service_type');
    })
};
