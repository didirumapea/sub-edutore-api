
exports.up = function(knex) {
    return knex.schema.alterTable('order_details', (table) => {
        table.renameColumn('issues_id', 'damage_type_id')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('order_details', (table) => {
        table.renameColumn('damage_type_id', 'issues_id')
    })
};
