
exports.up = function(knex, Promise) {
    return knex.schema.alterTable("sparepart", (table) => {
        table.dropColumn('brand');
        table.integer('damage_type_id')
            .unsigned()
            .after('id')
            .references('id')
            .inTable('damage_type')
            .onDelete('CASCADE')
            .index();
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable('sparepart', (table) => {
        table.string('brand')
            .after('name');
        table.dropColumn('damage_type_id');
    })
};
