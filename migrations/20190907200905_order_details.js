
exports.up = function(knex, Promise) {
    return knex.schema.createTable("order_details", (table) => {
        table.increments()
        table.integer('device_type_id')
            .unsigned()
            .notNullable()
            .references('id')
            .inTable('device_type')
            .onDelete('CASCADE')
            .index()
        table.integer('payment_provider_id')
        table.integer('payment_type_id')
        table.integer('issues_id').notNullable();
        table.integer('total_price');
        table.string('no_resi');
        table.json('location');
        table.enum('status', ['pending', 'progress', 'repair', 'complete']).defaultTo('pending');
        table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
        table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('order_details')
};
