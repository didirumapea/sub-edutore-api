
exports.up = function(knex) {
    return knex.schema.alterTable('order', (table) => {
        table.integer('isDeleted', 1)
            .notNullable()
            .after('payment_status')
            .defaultTo(0);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('order', (table) => {
        table.dropColumns('isDeleted')
    })
};
