const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('device_brand').del()
        .then(function () {
            // Inserts seed entries
            return knex('device_brand').insert([
                {
                    id: 1,
                    name: 'Brand Lain',
                    img_url: 'image_url a',
                    created_at: date.utc()
                },
                {
                    id: 2,
                    name: 'Samsung',
                    img_url: 'image_url ab',
                    created_at: date.utc()
                },
                {
                    id: 3,
                    name: 'Xiaomi',
                    img_url: 'image_url b',
                    created_at: date.utc()
                },
                {
                    id: 4,
                    name: 'Asus',
                    img_url: 'image_url c',
                    created_at: date.utc()
                },
                {
                    id: 5,
                    name: 'Apple',
                    img_url: 'image_url d',
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
