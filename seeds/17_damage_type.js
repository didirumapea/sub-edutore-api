const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('damage_type').del()
        .then(function () {
            // Inserts seed entries
            return knex('damage_type').insert([
                {
                    id: 1,
                    mst_damage_type_id: 1,
                    device_type_id: 5,
                    name: 'Tidak bisa nyala',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 2,
                    mst_damage_type_id: 2,
                    device_type_id: 3,
                    name: 'Sinyal lemah',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 3,
                    mst_damage_type_id: 3,
                    device_type_id: 2,
                    name: 'Masalah konektifitas lainnya',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 4,
                    mst_damage_type_id: 4,
                    device_type_id: 1,
                    name: 'Tidak ada suara sama sekali',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 5,
                    mst_damage_type_id: 2,
                    device_type_id: 5,
                    name: 'Tidak ada sinyal, tidak ada layanan, tidak ada jaringan',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 6,
                    mst_damage_type_id: 3,
                    device_type_id: 4,
                    name: 'Tidak dapat mengirm data/masalah pada koneksi usb',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 7,
                    mst_damage_type_id: 2,
                    device_type_id: 2,
                    name: 'sinyal tidak stabil',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 8,
                    mst_damage_type_id: 4,
                    device_type_id: 3,
                    name: 'suara menerima rendah/pelan',
                    price: 350000,
                    created_at: date.utc()
                },
                {
                    id: 9,
                    mst_damage_type_id: 1,
                    device_type_id: 1,
                    name: 'hp nyala tidak bisa masuk menu/logo',
                    price: 350000,
                    created_at: date.utc()
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
