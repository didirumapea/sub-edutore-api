const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('device_type').del()
        .then(function () {
            // Inserts seed entries
            return knex('device_type').insert([
                {
                    id: 1,
                    name: 'Zenfone',
                    resolution: '1400x500',
                    memory: '8GB',
                    processor: 'SNAPDRAGON',
                    devices_brand_id: 4,
                    created_at: date.utc()
                },
                {
                    id: 2,
                    name: 'S9',
                    resolution: '1400x500',
                    memory: '4GB',
                    processor: 'SNAPDRAGON',
                    devices_brand_id: 2,
                    created_at: date.utc()
                },
                {
                    id: 3,
                    name: 'Pocophone F1',
                    resolution: '1400x500',
                    memory: '6GB',
                    processor: 'SNAPDRAGON',
                    devices_brand_id: 3,
                    created_at: date.utc()
                },
                {
                    id: 4,
                    name: 'IPhone 5',
                    resolution: '1400x500',
                    memory: '6GB',
                    processor: 'SNAPDRAGON',
                    devices_brand_id: 5,
                    created_at: date.utc()
                },
                {
                    id: 5,
                    name: 'IPhone 6',
                    resolution: '1400x500',
                    memory: '6GB',
                    processor: 'SNAPDRAGON',
                    devices_brand_id: 5,
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
