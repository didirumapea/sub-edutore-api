const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('issues').del()
        .then(function () {
            // Inserts seed entries
            return knex('issues').insert([
                {
                    id: 1,
                    name: 'Baterai Bocor',
                    detail: 'detail a',
                    devices_type_id: 1,
                    price: 200000,
                    created_at: date.utc()
                },
                {
                    id: 2,
                    name: 'LCD Mati',
                    detail: 'detail b',
                    devices_type_id: 5,
                    price: 100000,
                    created_at: date.utc()
                },
                {
                    id: 3,
                    name: 'Mati Total',
                    detail: 'detail c',
                    devices_type_id: 4,
                    price: 500000,
                    created_at: date.utc()
                },
                {
                    id: 4,
                    name: 'Bootloop',
                    detail: 'detail d',
                    devices_type_id: 2,
                    price: 120000,
                    created_at: date.utc()
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
