const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('sparepart').del()
        .then(function () {
            // Inserts seed entries
            return knex('sparepart').insert([
                {
                    id: 1,
                    name: 'LCD',
                    damage_type_id: 2,
                    detail: 'detail a',
                    img_url: 'image_url a',
                    price: 200000,
                    devices_type_id: 1,
                    created_at: date.utc()
                },
                {
                    id: 2,
                    name: 'Keypad',
                    damage_type_id: 2,
                    detail: 'detail b',
                    img_url: 'image_url b',
                    price: 200000,
                    devices_type_id: 3,
                    created_at: date.utc()
                },
                {
                    id: 3,
                    name: 'Camera',
                    damage_type_id: 3,
                    detail: 'detail v',
                    img_url: 'image_url c',
                    price: 200000,
                    devices_type_id: 4,
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
