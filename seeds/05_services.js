const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('services').del()
        .then(function () {
            // Inserts seed entries
            return knex('services').insert([
                {
                    id: 1,
                    name: 'Baterai Bocor',
                    type: 'Phone',
                    detail: 'detail a',
                    img_url: 'image_url a',
                    devices_type_id: 1,
                    price: 200000,
                    service_type: 'on_site',
                    created_at: date.utc()
                },
                {
                    id: 2,
                    name: 'LCD Mati',
                    type: 'Laptop',
                    detail: 'detail b',
                    img_url: 'image_url b',
                    devices_type_id: 5,
                    price: 100000,
                    service_type: 'service_center',
                    created_at: date.utc()
                },
                {
                    id: 3,
                    name: 'Mati Total',
                    type: 'IPad',
                    detail: 'detail c',
                    img_url: 'image_url c',
                    devices_type_id: 4,
                    price: 500000,
                    service_type: 'on_site',
                    created_at: date.utc()
                },
                {
                    id: 4,
                    name: 'Bootloop',
                    type: 'NoteBook',
                    detail: 'detail',
                    img_url: 'image_url',
                    devices_type_id: 2,
                    price: 120000,
                    created_at: date.utc()
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
