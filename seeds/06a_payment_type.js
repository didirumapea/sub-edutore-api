const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('payment_type').del()
        .then(function () {
            // Inserts seed entries
            return knex('payment_type').insert([
                {
                    id: 1,
                    name: 'transfer',
                    payment_provider_id: 1,
                    created_at: date.utc()
                },
                {
                    id: 2,
                    name: 'credit card',
                    payment_provider_id: 2,
                    created_at: date.utc()
                },
                {
                    id: 3,
                    name: 'ovo',
                    payment_provider_id: 1,
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
