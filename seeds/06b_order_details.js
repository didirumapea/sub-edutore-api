const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('order_details').del()
        .then(function () {
            // Inserts seed entries
            return knex('order_details').insert([
                {
                    id: 1,
                    device_type_id: 1,
                    payment_provider_id: null,
                    payment_type_id: null,
                    issues_id: 1,
                    total_price: 700000,
                    no_resi: '',
                    location: '{"lat": "-6.1939318", "lng": "106.8228413"}',
                    status: 'pending',
                    created_at: date.utc()
                },
                {
                    id: 2,
                    device_type_id: 3,
                    payment_provider_id: null,
                    payment_type_id: null,
                    issues_id: 2,
                    total_price: 12300000,
                    no_resi: '',
                    location: '{"lat": "-6.1939318", "lng": "106.8228413"}',
                    status: 'pending',
                    created_at: date.utc()
                },
                {
                    id: 3,
                    device_type_id: 2,
                    payment_provider_id: null,
                    payment_type_id: null,
                    issues_id: 3,
                    total_price: 1200000,
                    no_resi: '',
                    location: '{"lat": "-6.1939318", "lng": "106.8228413"}',
                    status: 'pending',
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
