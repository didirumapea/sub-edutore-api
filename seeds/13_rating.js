const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('rating').del()
        .then(function () {
            // Inserts seed entries
            return knex('rating').insert([
                {
                    id: 1,
                    rating: 1,
                    order_id: 1,
                    user_id: 1,
                    technician_id: 1,
                    created_at: date.utc()
                },
                {
                    id: 2,
                    rating: 4,
                    order_id: 1,
                    user_id: 1,
                    technician_id: 1,
                    created_at: date.utc()
                },
                {
                    id: 3,
                    rating: 3,
                    order_id: 1,
                    user_id: 1,
                    technician_id: 1,
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
