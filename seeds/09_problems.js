const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('problems').del()
        .then(function () {
            // Inserts seed entries
            return knex('problems').insert([
                {
                    id: 1,
                    order_detail_id: 1,
                    sparepart_id: null,
                    services_id: 1,
                    created_at: date.utc()
                },
                {
                    id: 2,
                    order_detail_id: 2,
                    sparepart_id: null,
                    services_id: 2,
                    created_at: date.utc()
                },
                {
                    id: 3,
                    order_detail_id: 3,
                    sparepart_id: null,
                    services_id: 3,
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
