const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('admin').del()
        .then(function () {
            // Inserts seed entries
            return knex('admin').insert([
                {
                    id: 1,
                    name: 'Didi 1',
                    email: 'didi1@rimember.id',
                    password: '$2b$10$8NRgK5IJCOK10uGtumG0Mu4KnPLOE9LKThmnpBWwTv8jNPppZzlma',
                    phone: '0812938192891',
                    address: 'Jakarta',
                    activated: 1,
                    created_at: date.utc()
                },{
                    id: 2,
                    name: 'Didi 2',
                    email: 'didi2@rimember.id',
                    password: '$2b$10$9PkVddH1Zvi2tEG1N6md4OSSpPSo/EBe9YNFEd0ue0v9N87OcKucW',
                    phone: '0812938192891',
                    address: 'Jakarta',
                    activated: 1,
                    created_at: date.utc()
                },{
                    id: 3,
                    name: 'Didi 3',
                    email: 'didi3@rimember.id',
                    password: '$2b$10$NF1yB3Rt8vFHcdZJeQ62SOSzyWWA7OSaMGVxVqdDbSDwjM5spqwN6',
                    phone: '0812938192891',
                    address: 'Jakarta',
                    activated: 1,
                    created_at: date.utc()
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
