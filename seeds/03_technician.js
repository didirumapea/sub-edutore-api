const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('technician').del()
        .then(function () {
            // Inserts seed entries
            return knex('technician').insert([
                {
                    id: 1,
                    nik: '0129090909021',
                    name: 'Didi 1',
                    email: 'didi1@rimember.id',
                    password: '$2b$10$8NRgK5IJCOK10uGtumG0Mu4KnPLOE9LKThmnpBWwTv8jNPppZzlma',
                    phone: '0812938192891',
                    address: 'Jakarta',
                    service_center_id: '1',
                    tech_type: 'on_site',
                    created_at: date.utc()
                },{
                    id: 2,
                    nik: '0129090909021',
                    name: 'Didi 2',
                    email: 'didi2@rimember.id',
                    password: '$2b$10$9PkVddH1Zvi2tEG1N6md4OSSpPSo/EBe9YNFEd0ue0v9N87OcKucW',
                    phone: '0812938192891',
                    address: 'Jakarta',
                    service_center_id: '2',
                    tech_type: 'service_center',
                    created_at: date.utc()
                },{
                    id: 3,
                    nik: '0129090909021',
                    name: 'Didi 3',
                    email: 'didi3@rimember.id',
                    password: '$2b$10$NF1yB3Rt8vFHcdZJeQ62SOSzyWWA7OSaMGVxVqdDbSDwjM5spqwN6',
                    phone: '0812938192891',
                    address: 'Jakarta',
                    service_center_id: '2',
                    tech_type: 'on_site',
                    created_at: date.utc()
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
