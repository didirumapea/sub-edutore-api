const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('mst_damage_type').del()
        .then(function () {
            // Inserts seed entries
            return knex('mst_damage_type').insert([
                {
                    id: 1,
                    name: 'POWER',
                    created_at: date.utc()
                },
                {
                    id: 2,
                    name: 'CELLULER ACCESS',
                    created_at: date.utc()
                },
                {
                    id: 3,
                    name: 'CONNECTIVITY',
                    created_at: date.utc()
                },
                {
                    id: 4,
                    name: 'AUDIO',
                    created_at: date.utc()
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
