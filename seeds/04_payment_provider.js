const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('payment_provider').del()
        .then(function () {
            // Inserts seed entries
            return knex('payment_provider').insert([
                {
                    id: 1,
                    name: 'Mandiri',
                    address: 'Jakarta',
                    company_name: 'company_name',
                    type: 'type',
                    token: 'JDKS321032',
                    status: 1,
                    created_at: date.utc()
                },{
                    id: 2,
                    name: 'BCA',
                    address: 'Jakarta',
                    company_name: 'company_name',
                    type: 'type',
                    token: 'JDKS321032',
                    status: 1,
                    created_at: date.utc()
                },{
                    id: 3,
                    name: 'PANIN',
                    address: 'Jakarta',
                    company_name: 'company_name',
                    type: 'type',
                    token: 'JDKS321032',
                    status: 1,
                    created_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
