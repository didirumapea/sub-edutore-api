const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('service_center').del()
        .then(function () {
            // Inserts seed entries
            return knex('service_center').insert([
                {
                    id: 1,
                    name: 'SC KEBON SIRIH',
                    address: 'jl. pandeglang',
                    city: 'Jakarta',
                    coordinat: '{"lat": "123.109392109321", "lng": "19.231929058912"}',
                    phone: '0812938192891',
                    created_at: date.utc()
                },{
                    id: 2,
                    name: 'SC KEBON SIRIH 2',
                    address: 'jl. pandeglang',
                    city: 'Jakarta',
                    coordinat: '{"lat": "123.109392109321", "lng": "19.231929058912"}',
                    phone: '0812938192891',
                    created_at: date.utc()
                },{
                    id: 3,
                    name: 'SC KEBON SIRIH 3',
                    address: 'jl. pandeglang',
                    city: 'Jakarta',
                    coordinat: '{"lat": "123.109392109321", "lng": "19.231929058912"}',
                    phone: '0812938192891',
                    created_at: date.utc()
                },

            ]);
        }).catch((err) => {
            console.log(err)
        });
};
