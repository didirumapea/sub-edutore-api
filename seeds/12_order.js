const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex('order').del()
        .then(function () {
            // Inserts seed entries
            return knex('order').insert([
                {
                    id: 1,
                    user_id: 1,
                    no_order: 83092813801,
                    order_detail_id: 1,
                    technician_id: null,
                    service_type: 'on_site', //
                    payment_status: 'pending',
                    service_center_id: 1,
                    order_at: date.utc()
                },
                {
                    id: 2,
                    user_id: 2,
                    no_order: 82018302103,
                    order_detail_id: 2,
                    technician_id: null,
                    service_type: 'service_center', //
                    payment_status: 'pending',
                    service_center_id: 2,
                    order_at: date.utc()
                },
                {
                    id: 3,
                    user_id: 3,
                    no_order: 832018401902,
                    order_detail_id: 3,
                    technician_id: null,
                    service_type: 'service_center', //
                    payment_status: 'pending',
                    service_center_id: 1,
                    order_at: date.utc()
                },
            ]);
        }).catch((err) => {
            console.log(err)
        });
};
