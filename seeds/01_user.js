const date = require('../config/moment-date-format')
// console.log(date.timezoneFormat())
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
            id: 1,
            email: 'didirumapea@gmail.com',
            password: '$2b$10$2VMXFAaEpnkyq1A5PCiIOOGdZqm7jyNqXiydcaWqXTMlNbbg4d4Dy',
            name: 'didi',
            img_url: 'path',
            phone: '08129183921',
            register_type: 'manual',
            social_token: '392139291302180321',
            created_at: date.utc()
        },
        {
            id: 2,
            email: 'didi@rimember.id',
            password: '$2b$10$2VMXFAaEpnkyq1A5PCiIOOGdZqm7jyNqXiydcaWqXTMlNbbg4d4Dy',
            name: 'digan',
            img_url: 'path',
            phone: '08129183921',
            register_type: 'google',
            social_token: '38209183021830912',
            created_at: date.utc()
        },
        {
            id: 3,
            email: 'intercitymega@gmail.com',
            password: '$2b$10$2VMXFAaEpnkyq1A5PCiIOOGdZqm7jyNqXiydcaWqXTMlNbbg4d4Dy',
            name: 'cogan',
            img_url: 'path',
            phone: '08129183921',
            register_type: 'google',
            social_token: '3821903820183012151',
            created_at: date.utc()
        }
      ]);
    }).catch((err) => {
        console.log(err)
      });
};
