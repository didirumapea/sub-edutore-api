const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const config = require('../../../../config/config')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const jwt = require('jsonwebtoken')
// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 11;
const checkAuth = require('../../../../middleware/check-auth')
const slugify = require('slugify')

router.get('/', (req, res) => {
    res.send('We Are In BANNERS PARTNER Route')
})

// GET LIST PUBLISHER IMAGE
router.get('/list/publisher-name=:publisher_name/category=:category/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    // console.log(req.params)
    // let user_id = req.userData.id
    let sort = req.params.sort
    let page = req.params.page
    let limit = req.params.limit
    let col_sort = req.params.col_sort
    let publisher_name = req.params.publisher_name
    db.select(
        't1.*',
        't2.slug_name'
    )
        .from('banners_partner as t1')
        .leftJoin('publishers as t2', 't1.publishers_id', 't2.id')
        .where({
            category: req.params.category,
            is_publish: 1,
            slug_name: req.params.publisher_name
        })
        .orderBy(col_sort, sort)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Banner partner not found.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    data: paginator.data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data banner partner",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

// GET LIST PUBLISHER BG
router.get('/list/category=:category/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    // console.log(req.params)
    // let user_id = req.userData.id
    let sort = req.params.sort
    let page = req.params.page
    let limit = req.params.limit
    let col_sort = req.params.col_sort
    let publisher_name = req.params.publisher_name
    db.select(
        '*'
    )
        .from('banners_partner')
        .where({
            category: req.params.category,
            is_publish: 1
        })
        .orderBy(col_sort, sort)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Banner partner masih kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data banner partner",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

module.exports = router;