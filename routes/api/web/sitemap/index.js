const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const config = require('../../../../config/config')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const jwt = require('jsonwebtoken')
// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 11;
const checkAuth = require('../../../../middleware/check-auth')
const slugify = require('slugify')

router.get('/', (req, res) => {
    res.send('We Are In SITEMAP EDUTORE Route')
})

// GET
router.get('/list/modules/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    // console.log(req.params)
    // let user_id = req.userData.id
    db.select(
        'module_slug'
    )
        .from('modules')
        .where({
            is_publish: 1
        })
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Module masih kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data module",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

module.exports = router;