const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth')
const checkAuthTechnician = require('../../middleware/check-auth-technician')

// ----- WEB
const web_banners = require('./web/banners');
const web_sitemap = require('./web/sitemap');
const web_banners_partners = require('./web/banners/partners');
router.use('/web/sitemap', web_sitemap);
router.use('/web/banners', web_banners);
router.use('/web/banners/partners', web_banners_partners);



// ------------------------------------------------- CMS -----------------------------------------------
const cms_publishers = require('./cms/publishers');
const cms_banners = require('./cms/banners');
const cms_banners_partner = require('./cms/banners/partners');
const cms_customer = require('./cms/customer');
const cms_event = require('./cms/event');
const cms_client = require('./cms/client');
const cms_example = require('./cms/example');
router.use('/cms/publishers', cms_publishers);
router.use('/cms/banners', cms_banners);
router.use('/cms/banners/partners', cms_banners_partner);
router.use('/cms/customer', cms_customer);
router.use('/cms/event', cms_event);
router.use('/cms/client', cms_client);
router.use('/cms/example', cms_example);

// --- -------------------------------------------- APPS -----------------------------------------------


module.exports = router;