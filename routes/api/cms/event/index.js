const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const checkAuth = require('../../../../middleware/check-auth')
const date = require('../../../../plugins/moment-date-format')
// const firedb = require('../../../../plugins/firebase')
const cryptoRandomString = require('crypto-random-string');
const CryptoJS = require("crypto-js");
const shortUrl = require('node-url-shortener');
const path = require('path');
const readXlsxFile = require('read-excel-file/node');




// POST ORDER
router.post('/add-event', (req, res) => {
    // console.log(date.utc())
    // let timeOrder = moment().utc().format('YYYY-MM-DD HH:mm:ss')
    let bulk = {
        fid_customer: req.body.fid_customer,
        fid_event_group:  req.body.fid_event_group,
        event_unique_id: cryptoRandomString({length: 10, type: 'url-safe'}),
        event_title: req.body.event_title,
        event_desc: req.body.event_desc,
        event_dresscode: req.body.event_dresscode,
        event_date: req.body.event_date,
        event_time_start: req.body.event_time_start,
        event_time_end: req.body.event_time_end,
        event_building: req.body.event_building,
        event_address: req.body.event_address,
        event_city: req.body.event_city,
        event_url_video: req.body.event_url_video
    }

    db('event')
        .insert(bulk)
        .then(data => {
            res.json({
                success: true,
                message: "Add Event Berhasil.",
                // count: result.length,
                data: data,
            });

        })
        .catch((err) =>{
        // console.log('error at '+date.utc7())
        console.log(err)
        res.json({
            success: false,
            message: "Order failed.",
            // count: data.length,
            data: err,
        });
    });
});

module.exports = router;