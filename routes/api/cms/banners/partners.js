const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const { sendUploadToGCS } = require('../../../../middleware/google-cloud-storage')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const slugify = require('slugify')
const path = require('path')
const multer  = require('multer')
const randomstring = require("randomstring");
const storage = multer.diskStorage(
    {
        destination: '/mnt/cdn/sub-edutore-files/assets/images/banner',
        filename: function (req, file, cb ) {
            // console.log(file)
            let random = randomstring.generate({
                length: 20,
                // capitalization: 'uppercase',
            });
            let symbol = ''
            if (file.mimetype.split('/')[0] === 'video'){
                symbol = 'v'
            }else{
                symbol = 'p'
            }
            // console.log(file.mimetype.split('/')[1])
            // console.log(path.extname(file.originalname))
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            // cb( null, Date.now()+`-${symbol}${path.extname(file.originalname)}`);
            cb( null, random+`-${symbol}${path.extname(file.originalname)}`);
        }
    }
);

let upload = multer({ storage: storage })

const mlt = multer.memoryStorage({
    limits: 1024 * 1024
})

let Multer = multer({ storage: mlt })

router.get('/', (req, res) => {
    res.send('We Are In BANNERS PARTNER Route')
})

router.post('/upload-gcs', Multer.array('image'), sendUploadToGCS, (req, res, next) => {

    console.log(req.files[0]) // you will get the uploaded files name and url here
    delete req.files[0].buffer
    res.status(200).json({ files: req.files[0] });

})

router.get('/get-files', (req, res, next) => {

    // Imports the Google Cloud client library
    const {Storage} = require('@google-cloud/storage');

// Creates a client
    const storage = new Storage();

    async function listBuckets() {
        // Lists all buckets in the current project

        const [buckets] = await storage.getBuckets();
        console.log('Buckets:');
        buckets.forEach(bucket => {
            console.log(bucket.name);
        });
    }

    listBuckets().catch(console.error);
    res.status(200).json({ files: 'req.file' });

})

// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
        't2.name as publisher_name'
    )
        .from('banners_partner as t1')
        .leftJoin('publishers as t2', 't1.publishers_id', 't2.id')
        // .where(listWhere)
        .orderBy(req.params.col_sort, req.params.sort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Banner masih kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data banner",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

// GET BY ID
router.get('/list/banner-id=:id/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    db.select(
        't1.*',
        't2.name as publishers_name'
    )
        .from('banners_partner as t1')
        .innerJoin('publishers as t2', 't1.id', 't2.id')
        .where({'t1.id': req.params.id})
        .orderBy('t1.'+req.params.col_sort, req.paramssort)
        .paginate(req.params.limit, req.params.page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Banner is still empty.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data banner",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

// ADD
router.post('/add', upload.single('image'), (req, res) => {
    req.body.publishers_id = req.body.publishers_id === 'null' ? 0 : req.body.publishers_id
    // console.log(req.body)
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    if (req.file) {
        // console.log('Uploading file... '+req.file.filename);
        // let filename = req.file.filename;
        // let uploadStatus = 'File Uploaded Successfully';
        req.body.image = `${req.file.filename}`
        db.select(
            'name',
        )
            .from('banners_partner')
            .where({
                name: req.body.name,
            })
            .then((data) => {
                if (data.length === 0){
                    // console.log(detail)
                    db('banners_partner')
                        .insert(req.body)
                        .then(data => {
                            res.json({
                                success: true,
                                message: 'Add banner partner success',
                                count: data,
                                // data: result,
                            });
                        })
                        return true
                }else{
                    res.json({
                        success: false,
                        message: 'Banner partner already exist',
                        // count: data,
                        // data: result,
                    });

                }
            })
            .catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Add banner partner failed",
                    // count: data.length,
                    data: err,
                });
            });
    } else {
        console.log('No File Uploaded');
        // var filename = 'FILE NOT UPLOADED';
        // let uploadStatus = 'File Upload Failed';
        res.json({
            success: false,
            message: "upload image failed",
        });
    }
});

// ADD WITH GCS
router.post('/add-with-gcs', Multer.array('image'), sendUploadToGCS, (req, res) => {
    req.body.publishers_id = req.body.publishers_id === 'null' ? 0 : req.body.publishers_id
    // console.log(req.body)
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    if (req.files) {
        // console.log('Uploading file... '+req.file.filename);
        // let filename = req.file.filename;
        // let uploadStatus = 'File Uploaded Successfully';
        req.body.image = `${req.files[0].cloudStorageFileName}`
        db.select(
            'name',
        )
            .from('banners_partner')
            .where({
                name: req.body.name,
            })
            .then((data) => {
                if (data.length === 0){
                    // console.log(detail)
                    db('banners_partner')
                        .insert(req.body)
                        .then(data => {
                            res.json({
                                success: true,
                                message: 'Add banner partner success',
                                count: data,
                                // data: result,
                            });
                        })
                    return true
                }else{
                    res.json({
                        success: false,
                        message: 'Banner partner already exist',
                        // count: data,
                        // data: result,
                    });

                }
            })
            .catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Add banner partner failed",
                    // count: data.length,
                    data: err,
                });
            });
    } else {
        console.log('No File Uploaded');
        // var filename = 'FILE NOT UPLOADED';
        // let uploadStatus = 'File Upload Failed';
        res.json({
            success: false,
            message: "upload image failed",
        });
    }
});


// UPDATE
router.post('/update', upload.single('image'), (req, res) =>  {
    // console.log(req.body)
    req.file ? req.body.image = req.file.filename : req.body.image = req.body.old_image_name
    delete req.body.old_image_name
    // req.body.image = req.file.filename
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    db('banners_partner')
        .where('id', req.body.id)
        .update(req.body)
        .then(data => {
            res.json({
                success: true,
                message: "Update banners partner success.",
                // count: data.length,
                data: data,
            });
        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banners partner failed.",
                // count: data.length,
                data: err,
            });
        });
});

// UPDATE IS PUBLISH
router.post('/update-is-publish', (req, res) =>  {
    // console.log(req.body)
    db('banners_partner')
        .where('id', req.body.id)
        .update('is_publish', req.body.is_publish)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner partner status success.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banners partner status failed",
                // count: data.length,
                data: err,
            });
        });

});

// UPDATE IS PUBLISH
// router.get('/upload-gcs', (req, res) =>  {
//     res.json({
//         success: true,
//         message: "gcs done",
//     });
// });




module.exports = router;