const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const config = require('../../../../config/config')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const jwt = require('jsonwebtoken')
// bcrypt config
const bcrypt = require('bcrypt');
const saltRounds = 11;
const checkAuth = require('../../../../middleware/check-auth')
const slugify = require('slugify')
const path = require('path')
const multer  = require('multer')
const randomstring = require("randomstring");
const storage = multer.diskStorage(
    {
        destination: '/mnt/cdn/sub-edutore-files/assets/images/banner',
        filename: function (req, file, cb ) {
            // console.log(file)
            let random = randomstring.generate({
                length: 20,
                // capitalization: 'uppercase',
            });
            let symbol = ''
            if (file.mimetype.split('/')[0] === 'video'){
                symbol = 'v'
            }else{
                symbol = 'p'
            }
            // console.log(file.mimetype.split('/')[1])
            // console.log(path.extname(file.originalname))
            //req.body is empty...
            //How could I get the new_file_name property sent from client here?
            // cb( null, Date.now()+`-${symbol}${path.extname(file.originalname)}`);
            cb( null, random+`-${symbol}${path.extname(file.originalname)}`);
        }
    }
);
let upload = multer({ storage: storage })


router.get('/', (req, res) => {
    res.send('We Are In PUBLISHERS Route')
})

// GET
router.get('/list/page=:page/limit=:limit/column-sort=:col_sort/sort=:sort', (req, res) => {
    // console.log(req.params)
    // let user_id = req.userData.id
    let sort = req.params.sort
    let page = req.params.page
    let limit = req.params.limit
    let col_sort = req.params.col_sort
    db.select(
        '*'
    )
        .from('publishers')
        // .where(listWhere)
        // .orderBy('id', 'desc')
        .orderBy(col_sort, sort)
        .paginate(limit, page, true)
        .then(paginator => {
            // console.log(paginator)
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Publishers masih kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                res.json({
                    success: true,
                    message: "Success get data Publishers",
                    limit: paginator.per_page,
                    paginate: {
                        totalRow: paginator.total,
                        from: paginator.from,
                        to: paginator.to,
                        currentPage: paginator.current_page,
                        lastPage: paginator.last_page
                    },
                    data: paginator.data,
                });
            }
        });
});

// ADD
router.post('/add', upload.single('image'), (req, res) => {
    console.log(req.body)
    // req.body.user_id = req.userData.user_id
    // console.log('test')
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    if (req.file) {
        // console.log('Uploading file... '+req.file.filename);
        // let filename = req.file.filename;
        // let uploadStatus = 'File Uploaded Successfully';
        req.body.image = `${req.file.filename}`
        db.select(
            'name',
        )
            .from('banners')
            .where({
                name: req.body.name,
            })
            .then((data) => {
                if (data.length === 0){
                    // console.log(detail)
                    db('banners')
                        .insert(req.body)
                        .then(data => {
                            res.json({
                                success: true,
                                message: 'Add Banner success',
                                count: data,
                                // data: result,
                            });
                        })
                        return true
                }else{
                    res.json({
                        success: false,
                        message: 'Banner already exist',
                        // count: data,
                        // data: result,
                    });

                }
            })
            .catch((err) =>{
                console.log(err)
                res.json({
                    success: false,
                    message: "Add Banner failed",
                    // count: data.length,
                    data: err,
                });
            });
    } else {
        console.log('No File Uploaded');
        // var filename = 'FILE NOT UPLOADED';
        // let uploadStatus = 'File Upload Failed';
        res.json({
            success: false,
            message: "upload image failed",
        });
    }
});

// UPDATE
router.post('/update', upload.single('images'), (req, res) =>  {
    console.log(req.body)
    req.body.slug = slugify(req.body.name.toString(), {
        replacement: '-',  // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true,      // convert to lower case, defaults to `false`
        strict: false,     // strip special characters except replacement, defaults to `false`
    })
    db('banners')
        .where('id', req.body.id)
        .update(req.body)
        .then(data => {
            res.json({
                success: true,
                message: "Update banners success.",
                // count: data.length,
                data: data,
            });
        }).catch((err) =>{
        console.log(err)
        res.json({
            success: false,
            message: "Update banners failed.",
            // count: data.length,
            data: err,
        });
    });
});

// UPDATE IS PUBLISH
router.post('/update-is-publish', (req, res) =>  {
    // console.log(req.body)
    db('banners')
        .where('id', req.body.id)
        .update('is_publish', req.body.is_publish)
        .then(data => {
            res.json({
                success: true,
                message: "Update banner status succedd.",
                count: data.length,
                data: data,
            });

        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Update banners status failed",
                // count: data.length,
                data: err,
            });
        });

});


module.exports = router;