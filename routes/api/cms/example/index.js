const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
moment.locale('id')
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const checkAuth = require('../../../../middleware/check-auth')
const date = require('../../../../plugins/moment-date-format')
const mailer = require('../../../../plugins/mailing')
// const firedb = require('../../../../plugins/firebase')
const cryptoRandomString = require('crypto-random-string');
const CryptoJS = require("crypto-js");
const shortUrl = require('node-url-shortener');
const path = require('path');
const readXlsxFile = require('read-excel-file/node');

const twilio = require('twilio');
const accountSid = 'AC313562a376da229819acd2f44ffd6db7';
const authToken = '8c969342a2393f868d44de35071b5d04';
const client = new twilio(accountSid, authToken);
const request = require('request');
// const objCodec = require('object-encode');

router.get('/twilio-whatsapp', (req, res) => {

    client.messages
        .create({
            body: 'Another From twilio',
            from: 'whatsapp:+17747736080',
            to: 'whatsapp:+6281298943893'
        })
        .then(message => console.log(message.sid))
        .done();

    res.json({
        success: true,
        message: 'Post Created',
        // data: url
    })
});
router.get('/twilio-whatsapp/status-callback-url', (req, res) => {

    // console.log(res)

    res.json({
        success: true,
        message: 'Post Created',
        // data: url
    })
});

router.get('/chat-api-test-send-message', (req, res) => {

    // console.log(res)
    // no mas panda = 628176789682
    // my number = 6281298943893
    // my mother = 6282111035733
    // eva = 6285329303977
    // mamih = 628176789681
    // brother = 6285711337147
    let myJSONObject = {
        "phone": "6282111035733",
        "body":
            `*Yang Terhormat Mr/Mrs {{Darmawan}}*
Bapak dan Ibu
{{Bambang Adi Saputra}}
bersama dengan
Bapak dan Ibu
{{Surya Sujatmiko}}\n
Dengan hormat mengundang anda untuk hadir dalam acara pernikahan putra dan putri kami
*{{Dilan Saputra dan Milea Sujatmiko}}*\n
Yang akan diadakan di
*{{Bandung, Minggu 27 Juni 2020}}*
*Pukul {{14:00}}*
*{{Auditorium Hotel Art Dimair, Bandung}}*\n
Untuk keperluan reservasi, silahkan isi data anda pada link berikut ini :\n
https://theinviteme.com/attendance/1/w9eF~1zeodhRTjfWPnFH\n
https://bitly.com\n
Kehadiran anda sangat kami harapkan dan kami hargai selaku yang merayakan\n
*Hormat Kami,*\n
*Invitation Team of*
*{{Dilan Saputra dan Milea Sujatmiko}}*`

    }
    // console.log(myJSONObject)
    request({
        url: "https://eu103.chat-api.com/instance103729/sendMessage?token=ks07dlqjwotspv3g",
        method: "POST",
        json: true,   // <--Very important!!!
        body: myJSONObject
    }, function (error, response, body){
        console.log(response);
    });

    res.json({
        success: true,
        message: 'Post Created',
        // data: url
    })
});

router.get('/generate-link-invitation', (req, res) => {
    let url = []
    for (let x = 0; x < 100; x++){
        let url_safe = cryptoRandomString({length: 20, type: 'url-safe'})
        let number = cryptoRandomString({length: 4, type: 'numeric'})
        url.push(`https://eo.com/attendation/1/${url_safe}`)
    }
    // cryptoRandomString({length: 20, type: 'url-safe'});

    res.json({
        success: true,
        message: 'Post Created',
        data: url
    })

});
router.get('/generate-link-invitation-crypto', (req, res) => {
    // Encrypt
    let objPlainText = {
        name: 'darmawan',
        id: '4',
        telp: '0818392819892',
    }
    let ciphertext = CryptoJS.AES.encrypt(JSON.stringify(objPlainText), 'secret key 123').toString();
    let e64 = CryptoJS.enc.Base64.parse(ciphertext);
    let result = e64.toString(CryptoJS.enc.Hex)
    console.log('Enc: '+ciphertext)
// Decrypt
    let bytes  = CryptoJS.AES.decrypt(ciphertext, 'secret key 123');
    let originalText = bytes.toString(CryptoJS.enc.Utf8);

    console.log(originalText); // 'my message'
    // cryptoRandomString({length: 20, type: 'url-safe'});

    res.json({
        success: true,
        message: 'Post Created',
        data: {
            resEnc: result
        }
    })

});
router.get('/short-url', (req, res) => {

    shortUrl.short('https://edutore.com/modules/bimbel-sinotif-kelas-4-sd-modul-matematika?type=0', function(err, url){
        console.log(url);
    });

    res.json({
        success: true,
        message: 'Post Created',
        data: {
            resEnc: 'result'
        }
    })

});
router.get('/excel-reader2', (req, res) => {
    let appDir = path.dirname(require.main.filename);
    if(typeof require !== 'undefined') XLSX = require('xlsx');
    let workbook = XLSX.readFile(appDir+'/public/data/sample-data.xlsx');

    // console.log(workbook)

    let first_sheet_name = workbook.SheetNames[0];
    let address_of_cell = 'A1';

    /* Get worksheet */
    let worksheet = workbook.Sheets[first_sheet_name];

    /* Find desired cell */
    let desired_cell = worksheet[address_of_cell];

    /* Get the value */
    let desired_value = (desired_cell ? desired_cell.v : undefined);

    console.log(worksheet)
    // console.log(appDir+'/public/data/sample-data.xlsx')
    res.json({
        success: true,
        message: 'Post Created',
        data: {
            resEnc: 'result'
        }
    })

});
router.get('/excel-reader', (req, res) => {
    let bulk = []

    let appDir = path.dirname(require.main.filename);
    // appDir+'/public/data/sample-data.xlsx
    readXlsxFile(appDir+'/public/data/sample-data.xlsx').then((rows) => {
        rows.shift()
        rows.forEach((element, index) => {
            let xdata = {
                fid_event: '',
                guest_phone: element[0],
                guest_name: element[1],
                guest_max_pax: element[2],
                guest_unique_id: cryptoRandomString({length: 20, type: 'url-safe'})
            }
            // console.log(index+1)
            bulk.push(xdata)
            // console.log(element[2])
        })
        db('guestlist')
            .insert(bulk)
            .then(data => {
                res.json({
                    success: true,
                    message: "Add Reservation berhasil.",
                    // count: result.length,
                    data: data,
                });
            })
            .catch((err) =>{
                // console.log('error at '+date.utc7())
                console.log(err)
                res.json({
                    success: false,
                    message: "insert Reservation failed.",
                    // count: data.length,
                    data: err,
                });
            });
        // `rows` is an array of rows
        // each row being an array of cells.
    })
    // console.log()
    // console.log(appDir+'/public/data/sample-data.xlsx')
    // res.json({
    //     success: true,
    //     message: 'Post Created',
    //     data: {
    //         resEnc: 'result'
    //     }
    // })

});
router.get('/test-send-email', (req, res) => {

    // mailer.sendEmailGmail('nice')
    db.select(
        't1.id',
        't1.guest_email',
        't1.guest_name',
        't1.guest_unique_id',
        't1.guest_partner_name',
        't1.id',
        't3.group_title as event_name',
        't2.id as event_id',
        't2.*',

    )
        .from('guestlist as t1')
        .innerJoin('event as t2', 't1.fid_event', 't2.id')
        .innerJoin('mst_event as t3', 't2.fid_event_group', 't3.id')
        .where('t1.id', 1)
        .andWhere('t3.isDeleted', '0')
        // .orderBy('guest_number', 'desc')
        // .limit(1)
        .then((data) => {
            if (data.length > 0){
                let guest = []
                let result = data[0]
                console.log(moment(result.event_time_start, "HH:mm:ss").format('HH:mm'))
                if (result.guest_partner_name !== null){
                    guest.push(result.guest_name)
                    guest.push(result.guest_partner_name)
                }else{
                    guest.push(result.guest_name)
                }
                result.event_date = moment(result.event_date).format('dddd DD MMMM YYYY')
                result.event_time_start = moment(result.event_time_start, "HH:mm:ss").format('HH:mm')
                result.guest_name = guest
                result.url = `https://theinviteme.com/barcode/${result.event_id}`
                res.json({
                    success: true,
                    message: 'Get Event Success!',
                    data: result
                })
                // mailer.sendEmailGmail(data)
            }else{
                res.json({
                    success: false,
                    message: 'Event Not Found!',
                    // data: {
                    //     resEnc: 'Nope & Rory'.split('&').map(item => item.trim())
                    // }
                })
            }
        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Get Event Failed.",
                // count: data.length,
                data: err,
            });
        });
});
router.get('/cryptojs-dec', (req, res) => {

    // Decrypt
    let decrypt  = CryptoJS.AES.decrypt(req.body.dec, req.body.passphrase);
    let decryptedData = decrypt.toString(CryptoJS.enc.Utf8);
    // console.log(decrypt)
    // console.log(originalText)

    res.json({
        success: true,
        message: 'Encrypted Success',
        data: {
            decryptedData
        }
    })

});
router.get('/cryptojs-enc', (req, res) => {
    // Encrypt
    // var hash = CryptoJS.SHA3("invite.me2020").toString();
    // var ciphertext = CryptoJS.AES.encrypt('Message', hash).toString()
    // console.log(encrypt('test_cchipper', 'inviteme.2020'))
    // console.log(ciphertext)
    var encrypted = CryptoJS.AES.encrypt(req.body.enc, req.body.passphrase);
    encrypted = encrypted.toString();
    console.log("Cipher text: " + encrypted);

    res.json({
        success: true,
        message: 'Generate Barcode success',
        data: {
            encrypted
            // resEnc: ciphertextPart4
        }
    })
})
router.get('/generate-passphrase', (req, res) => {
    const Cryptr = require('cryptr');
    const cryptr = new Cryptr('inviteme2020');

    const encryptedString = cryptr.encrypt('theinviteme20xx');
    // const decryptedString = cryptr.decrypt(encryptedString);
    // Encrypt
    // var ciphertext = CryptoJS.AES.encrypt(req.body.enc, 'invite.me2020').toString();


    res.json({
        success: true,
        message: 'Generate Passphrase success',
        data: {
            passphrase: encryptedString
        }
    })

});


// GET ORDER
router.get('/get/order/page=:page/limit=:limit/sort=:sort', checkAuth, (req, res) => {
    // console.log(req.userData)
    let user_id = req.userData.id
    let sort = req.params.sort
    let page = req.params.page
    let limit = req.params.limit
    // let remember_token = req.body.remember_token
    db.select(
        't1.id',
        't1.service_type',
        't2.files',
        't5.id as technician_id',
        't5.name as technician',
        't6.name as service_center',
        't6.address as service_center_address',
        't6.coordinat',
        't1.no_order',
        'order_detail_id',
        'payment_status',
        'order_at',
        't7.id as damage_type_id',
        't7.name as damage_name',
        't7.price as damage_price',
        't8.name as mst_damage_name',
        't3.name as device_type',
        't4.name as device_brand',
        'payment_provider_id',
        'payment_type_id',
        'total_price',
        'location',
        'status',
        't9.transaction_id',
        't9.paid_date',
    )
        .from('order as t1')
        .innerJoin('order_details as t2', 't1.order_detail_id', 't2.id')
        .innerJoin('device_type as t3', 't2.device_type_id', 't3.id')
        .innerJoin('device_brand as t4', 't3.devices_brand_id', 't4.id')
        .leftJoin('technician as t5', 't1.technician_id', 't5.id')
        .leftJoin('service_center as t6', 't1.service_center_id', 't6.id')
        .leftJoin('damage_type as t7', 't2.damage_type_id', 't7.id')
        .leftJoin('mst_damage_type as t8', 't7.mst_damage_type_id', 't8.id')
        .leftJoin('transaction as t9', 't1.trans_id', 't9.id')
        .where('user_id', user_id)
        .orderBy('id', sort)
        .paginate(limit, page, true)
        .then(paginator => {
            paginator.data.forEach((element, index) => {
                paginator.data[index].coordinat = JSON.parse(element.coordinat)
                paginator.data[index].files = JSON.parse(element.files)
            })
            if (paginator.data.length === 0){
                res.json({
                    success: true,
                    message: "Order Masih Kosong.",
                    count: paginator.data.length,
                    // current_page: paginator.current_page,
                    // limit: paginator.data.length,
                    // sortBy: sortBy,
                    // data: data,
                });
            }else{
                // console.log(data[0].order_detail_id)
                db.select(
                    't3.name as service_name'
                )
                    .from('problems as t1')
                    .innerJoin('order_details as t2', 't1.order_detail_id', 't2.id')
                    .innerJoin('services as t3', 't1.services_id', 't3.id')
                    .where('t2.id', paginator.data[0].order_detail_id)
                    .then((data2) => {
                        res.json({
                            success: true,
                            message: "sukses ambil data order",
                            current_page: paginator.current_page,
                            limit: paginator.data.length,
                            paginate: {
                                totalRow: paginator.total,
                                from: paginator.from,
                                to: paginator.to,
                                currentPage: paginator.current_page,
                                lastPage: paginator.last_page
                            },
                            sortBy: sort,
                            data: paginator.data,
                        });
                    })

            }
        });
    // res.json({
    //     message: 'Post Created',
    //     authData
    // })

});
// POST ORDER
router.post('/order', checkAuth, (req, res) => {
    // console.log(date.utc())
    let timeOrder = moment().utc().format('YYYY-MM-DD HH:mm:ss')
    let order = {
        user_id: req.userData.id,
        no_order: moment(timeOrder).format('X'), // GMT +0
        service_center_id: req.body.order.service_center_id,
        payment_status: req.body.order.payment_status,
        order_at: timeOrder,
        service_type: req.body.order.service_type
    }

    let orderDetails = {
        // user_id: req.userData.id,
        device_type_id: req.body.orderDetails.device_type_id,
        damage_type_id: req.body.orderDetails.damage_type_id,
        payment_provider_id: req.body.orderDetails.payment_provider_id,
        payment_type_id: req.body.orderDetails.payment_type_id,
        total_price: req.body.orderDetails.total_price,
        location: JSON.stringify(req.body.orderDetails.location),
        status: req.body.orderDetails.status,
        created_at: date.utc(),
        files: JSON.stringify(req.body.orderDetails.files)
    }
    // if (req.body.orderDetails.isFilesAdded){
    //     orderDetails.files = {
    //         photo: req.body.order.no_order+'_',
    //         video: req.body.order.no_order
    //     }
    // }
    console.log(orderDetails)

    db('order_details')
        .insert(orderDetails)
        .then(data => {
            order.order_detail_id = data
            db('order')
                .insert(order)
                .then(data3 => {
                    res.json({
                        success: true,
                        message: "Order berhasil.",
                        // count: result.length,
                        data: {
                            no_order: order.no_order
                        },
                    });
                })

        }).catch((err) =>{
        // console.log('error at '+date.utc7())
        console.log(err)
        res.json({
            success: false,
            message: "Order failed.",
            // count: data.length,
            data: err,
        });
    });
});
// TEST FIREBASE
router.get('/test/firebase', checkAuth, (req, res) => {
    // console.log(firedb)
    // WRITE
    // firedb.ref('progress-loading/'+'1579120792').push({
    //   messageText: '94%',
    //   messageTime: parseInt(moment().format('x')),
    // })
    // READ
    // let data = db.ref('chat/1579120792')
    // data.on('value', function(snapshot) {
    //     console.log(snapshot.val())
    //     // self.chatMessag`es = snapshot.val();
    // });
    res.json({
        message: 'Post Created',
    })
});


module.exports = router;