const express = require('express');
const router = express.Router();
const db = require('../../../../database').db; // as const knex = require('knex')(config);
const moment = require('moment/moment');
const setupPaginator = require('knex-paginator');
setupPaginator(db);
const cryptoRandomString = require('crypto-random-string');
const path = require('path');
const readXlsxFile = require('read-excel-file/node');
const CryptoJS = require("crypto-js");

//
router.get('/add-list-invitation', (req, res) => {
    let bulk = []

    let appDir = path.dirname(require.main.filename);
    // appDir+'/public/data/sample-data.xlsx
    readXlsxFile(appDir+'/public/data/sample-data.xlsx').then((rows) => {
        rows.shift()
        rows.forEach((element, index) => {
            let xdata = {
                fid_event: 1,
                guest_phone: element[0],
                guest_name: element[1],
                guest_max_pax: element[2],
                guest_unique_id: cryptoRandomString({length: 20, type: 'url-safe'})
            }
            // console.log(index+1)
            bulk.push(xdata)
            // console.log(element[2])
        })
        db('guestlist')
            .insert(bulk)
            .then(data => {
                res.json({
                    success: true,
                    message: "Add Reservation berhasil.",
                    // count: result.length,
                    data: data,
                });
            })
            .catch((err) =>{
                // console.log('error at '+date.utc7())
                console.log(err)
                res.json({
                    success: false,
                    message: "insert Reservation failed.",
                    // count: data.length,
                    data: err,
                });
            });
        // `rows` is an array of rows
        // each row being an array of cells.
    })
    // console.log()
    // console.log(appDir+'/public/data/sample-data.xlsx')
    // res.json({
    //     success: true,
    //     message: 'Post Created',
    //     data: {
    //         resEnc: 'result'
    //     }
    // })

});
router.post('/update-attend', (req, res) => {
    // console.log(req.body)
    let details = {
        guest_number: null,
        guest_email: req.body.guest_email,
        guest_partner_name: req.body.guest_partner_name,
        guest_pax: req.body.guest_pax,
        attend: req.body.attend,
    }

    db.select(
        'guest_number'
    )
        .from('guestlist as t1')
        .innerJoin('event as t2', 't1.fid_event', 't2.id')
        .where('t2.event_unique_id', req.body.event_unique_id)
        .andWhere('t1.id', req.body.id)
        .andWhere('t1.guest_unique_id', req.body.guest_unique_id)
        .then((data3) => {
            // console.log(data3.length)
            if (data3.length === 0){
                res.json({
                    success: false,
                    message: "Guest not found",
                });
            }else{
                if (data3[0].guest_number === null){
                    db.select(
                        'guest_number',
                    )
                        .from('guestlist as t1')
                        .innerJoin('event as t2', 't1.fid_event', 't2.id')
                        .where('event_unique_id', req.body.event_unique_id)
                        .orderBy('guest_number', 'desc')
                        .limit(1)
                        .then((data) => {
                            // console.log(data)
                            details.guest_number = data[0].guest_number + 1
                            db('guestlist as t1')
                                .innerJoin('event as t2', 't1.fid_event', 't2.id')
                                .where('t1.id', req.body.id)
                                .andWhere('event_unique_id', req.body.event_unique_id)
                                .andWhere('guest_unique_id', req.body.guest_unique_id)
                                .andWhere('guest_number', null)
                                .update(details)
                                .then(data2 => {
                                    if (data2 === 0){
                                        res.json({
                                            success: false,
                                            message: "Guest not found",
                                        });
                                    }else{
                                        resultUpdateAttend(req, res, {data, email: details.guest_email})
                                    }
                                })
                                .catch((err) =>{
                                    console.log(err)
                                    res.json({
                                        success: false,
                                        message: "Update guestlist failed.",
                                        // count: data.length,
                                        data: err,
                                    });
                                });
                        })
                }else{
                    res.json({
                        success: false,
                        message: "Guest sudah melakukan Attendance",
                        // count: data.length,
                        // data: err,
                    });
                }
            }


        })
});
router.post('/update-reservation', (req, res) => {
    let details = {
        rsvp: req.body.rsvp,
    }

    db.select(
        'id',
        'guest_name',
        'guest_number',
        'attend',
        'rsvp',
    )
        .from('guestlist')
        .where({
            id: req.body.guest_id,
            guest_unique_id: req.body.guest_unique_id,
            fid_event: req.body.fid_event
        })
        .andWhere('guest_unique_id', req.body.guest_unique_id)
        .andWhere('fid_event', req.body.fid_event)
        .then((data3) => {
            if (data3.length === 0){
                res.json({
                    success: false,
                    message: "Guest not found",
                });
            }else{
                // console.log(data3)
                if (data3[0].rsvp === null){
                    db.select(
                        'guest_number',
                    )
                        .from('guestlist')
                        .where('fid_event', req.body.fid_event)
                        .orderBy('guest_number', 'desc')
                        .limit(1)
                        .then((data) => {
                            db('guestlist')
                                .where({
                                    id: req.body.guest_id,
                                    guest_unique_id: req.body.guest_unique_id,
                                    fid_event: req.body.fid_event
                                })
                                .update(details)
                                .then(data2 => {
                                    if (data2 === 0){
                                        res.json({
                                            success: false,
                                            message: "Guest not found",
                                        });
                                    }else{
                                        data3[0].rsvp = req.body.rsvp
                                        res.json({
                                            success: true,
                                            message: "Update reservation success.",
                                            // count: data.length,
                                            data: data3,
                                        });
                                    }
                                    // console.log(data2)

                                })
                                .catch((err) =>{
                                    console.log(err)
                                    res.json({
                                        success: false,
                                        message: "Update guestlist failed.",
                                        // count: data.length,
                                        data: err,
                                    });
                                });
                        })
                }else{
                    res.json({
                        success: false,
                        message: "Reservation already used",
                        // count: data.length,
                        // data: data3,
                    });
                }

            }
        })
});
router.get('/invited/event_unique_id=:event_unique_id/guest_unique_id=:guest_unique_id', (req, res) => {

    // console.log(req.headers['x-api-key'].length)
    // if (req.headers['x-api-key'].length === 0){
    //     return res.json({
    //         success: false,
    //         message: 'x-api-key failed compare',
    //     })
    // }
    db.select(
        'id',
    )
        .from('event')
        .where('event_unique_id', req.params.event_unique_id)
        .then((data) => {
            // console.log(JSON.stringify(data))
            if (data.length > 0){
                db.select(
                    't1.id as guest_id',
                    'guest_phone',
                    'guest_name',
                    'guest_max_pax',
                    'guest_number',
                    'attend',
                    'rsvp',
                    't2.id as event_id',
                    't2.event_title',
                    't2.event_desc',
                    't2.event_date',
                )
                    .from('guestlist as t1')
                    .innerJoin('event as t2', 't1.fid_event', 't2.id')
                    .where('guest_unique_id', req.params.guest_unique_id)
                    .andWhere('event_unique_id', req.params.event_unique_id)
                    .then((data) => {
                        // console.log(JSON.stringify(data))
                        if (data.length > 0){
                            res.json({
                                success: true,
                                message: 'Success get guest details.',
                                data: data
                                // data: encrypted
                            })
                        } else {
                            res.json({
                                success: false,
                                message: 'Guest not found.',
                                // data: data
                            })
                        }
                    })
            } else {
                res.json({
                    success: false,
                    message: 'Event not found.',
                    // data: data
                })
            }

        })
        .catch((err) => {
            console.log(err)
            res.json({
                success: false,
                message: 'Something Error',
                err: err
            })
        })



});

resultUpdateAttend = (req, res, response) => {
    // mailer.sendEmailGmail('nice')
    console.log(response.email)
    db.select(
        't1.id',
        't1.guest_email',
        't1.guest_name',
        't1.guest_unique_id',
        't1.guest_partner_name',
        't1.id',
        't3.group_title as event_name',
        't2.id as event_id',
        't2.*',

    )
        .from('guestlist as t1')
        .innerJoin('event as t2', 't1.fid_event', 't2.id')
        .innerJoin('mst_event as t3', 't2.fid_event_group', 't3.id')
        .where('t1.id', 1)
        .andWhere('t3.isDeleted', '0')
        // .orderBy('guest_number', 'desc')
        // .limit(1)
        .then((data) => {
            if (data.length > 0){
                let guest = []
                let result = data[0]
                // console.log(moment(result.event_time_start, "HH:mm:ss").format('HH:mm'))
                if (result.guest_partner_name !== null){
                    guest.push(result.guest_name)
                    guest.push(result.guest_partner_name)
                }else{
                    guest.push(result.guest_name)
                }
                result.event_date = moment(result.event_date).format('dddd DD MMMM YYYY')
                result.event_time_start = moment(result.event_time_start, "HH:mm:ss").format('HH:mm')
                result.guest_name = guest
                result.guest_email = response.email
                result.url = `https://theinviteme.com/barcode/${result.event_id}`
                res.json({
                    success: true,
                    message: 'Get Event Success!',
                    last_guest_number: response.data[0].guest_number,
                    new_guest_number: response.data[0].guest_number + 1,
                    data: result,
                })
                // mailer.sendEmailGmail(data)
            }else{
                res.json({
                    success: false,
                    message: 'Event Not Found!',
                    // data: {
                    //     resEnc: 'Nope & Rory'.split('&').map(item => item.trim())
                    // }
                })
            }
        })
        .catch((err) =>{
            console.log(err)
            res.json({
                success: false,
                message: "Get Event Failed.",
                // count: data.length,
                data: err,
            });
        });
}

module.exports = router;

