module.exports = {
    jwtSecretKeyClient: 'client-inviteme2020',
    version: "v1",
    appversion: "1.0.0",
    playstoreUrl: "https://play.google.com",
    Secret: "AplikasiModalin2018",
    port: "8080",
    assetPath: "/mnt/cdn/",
    cdn: "http://cdn.jastipapps.com/assets/",
    Session: {
        session: false
    },
    // SANDBOX KEY
    developmentMidtrans: {
        serverKey : 'SB-Mid-server-0eC29isRx6oEUL3B9IGhdCJB',
        clientKey : 'SB-Mid-client-vjwy8BI6E55UbjCk'
    },
    productionMidtrans: {
        serverKey : 'Mid-server-6JlX4fH9Aexr1C_nJGYvPWRP',
        clientKey : 'Mid-client-kUvLY241mzHcWYrS',
        headers: {
            Accept: 'application/json',
            content_type: 'application/json',
            Authorization: 'Basic TWlkLXNlcnZlci02SmxYNGZIOUFleHIxQ19uSkdZdlBXUlA6' // base64(serverKey)
        }
    }
};