// const jwt = require('jsonwebtoken')
// const config = require('../config/config')

module.exports = (req, res, next) => {
    try{
        if (req.headers['on-project'] !== process.env.H_ON_PROJECT){
            return res.status(401).json({
                // message: 'auth Failed'
                message: 'You are not allowed here'
            })
        }else{
            if (req.headers['signature-key'] !== process.env.H_SIGN_KEY){
                return res.status(401).json({
                    // message: 'auth Failed'
                    message: 'You are in restristic area.'
                })
            }else{
                next();
            }
        }
        // console.log(req.headers['on-project'])
        // const token = req.headers.authorization.split(' ')[1]
        // req.userData = jwt.verify(token, config.jwtSecretKey);
    }catch (error) {
        let err = null
        if (error.message === 'jwt expired'){
            err = 'Token Expired'
        }else{
            err = 'Auth Failed'
        }
        return res.status(401).json({
            // message: 'auth Failed'
            message: err
        })
    }
}