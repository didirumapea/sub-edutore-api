const { Storage } = require('@google-cloud/storage');
const randomstring = require("randomstring");
const path = require('path')
// const multer  = require('multer')
const CLOUD_BUCKET = process.env.CLOUD_BUCKET;
const storage = new Storage({
    projectId: process.env.GCLOUD_PROJECT_ID,
    keyFilename: process.env.KEYFILE_PATH
})
const bucket = storage.bucket(CLOUD_BUCKET)
const path_folder = 'sub-edutore-files/banners/'

const getPublicUrl = (filename) => {
    return `https://storage.googleapis.com/${CLOUD_BUCKET}/${path_folder}${filename}`
}

const sendUploadToGCS = (req, res, next) => {
    if (!req.files) {
        return next()
    }
    let promises = [];
    req.files.forEach((image, index) => {
        let random = randomstring.generate({
            length: 20,
            // capitalization: 'uppercase',
        });
        // const gcsname = req.headers.decode.username + Date.now() + image.originalname
        // const gcsname = Date.now() +'-'+ image.originalname

        let gcsname = random + path.extname(image.originalname)
        req.body.old_image_name !== undefined ? gcsname = req.body.old_image_name : ''
        const file = bucket.file(`${path_folder + gcsname}`)

        const promise = new Promise((resolve, reject) => {
            const stream = file.createWriteStream({
                metadata: {
                    contentType: image.mimetype
                }
            });

            stream.on('finish', async () => {
                try {
                    req.files[index].cloudStorageObject = `${path_folder+gcsname}`
                    req.files[index].cloudStorageFileName = `${gcsname}`
                    await file.makePublic()
                    req.files[index].cloudStoragePublicUrl = getPublicUrl(`${gcsname}`)
                    resolve();
                } catch (error) {
                    reject(error)
                }
            });

            stream.on('error', (err) => {
                req.files[index].cloudStorageError = err
                reject(err)
            });

            stream.end(image.buffer);
        })

        promises.push(promise)
    });

    Promise.all(promises)
        .then(_ => {
            promises = [];
            next();
        })
        .catch(next);
}

module.exports = {
    sendUploadToGCS,
    // multer
}